import { assert } from 'assert'
import { describe, it } from 'mocha'
import { determineRemaining, determineStatusValue, validateEta, validateSize } from '../app/util.js'

describe('Status', function () {
  const tests = [
    { status: 'DOWNLOADING', expected: 1 },
    { status: 'COMPLETING', expected: 130 },
    { status: 'SAVING', expected: 130 },
    { status: 'IN_QUEUE', expected: 64 },
    { status: 'WAITING', expected: 64 },
    { status: 'COMPLETED', expected: 136 },
    { status: 'SEEDING', expected: 136 },
    { status: 'PREPARING_SEED', expected: 136 },
    { status: 'OTHER', expected: 16 }
  ]
  tests.forEach(({ status, expected }) => {
    it(`should return ${expected} when the value is ${status}`, function () {
      assert.strictEqual(determineStatusValue(status), expected)
    })
  })
})

describe('Validate ETA', function () {
  const tests = [
    { eta: null, expected: 0 },
    { eta: 0, expected: 0 },
    { eta: 10, expected: 10 }
  ]
  tests.forEach(({ eta, expected }) => {
    it(`should return ${expected} when the value is ${eta}`, function () {
      assert.strictEqual(validateEta(eta), expected)
    })
  })
})

describe('Validate Availability', function () {
  const tests = [
    { total: 0, availability: null, expected: 0 },
    { total: 10, availability: 0, expected: 0 },
    { total: 10, availability: 10, expected: 10 }
  ]
  tests.forEach(({ availability, total, expected }) => {
    it(`should return ${expected} when the value is ${availability}`, function () {
      assert.strictEqual(validateEta(availability, total), expected)
    })
  })
})

describe('Determine Remaining', function () {
  const tests = [
    { size: 0, downloaded: null, status: 'COMPLETED', expected: 0 },
    { size: 10, downloaded: 0, status: 'COMPLETED', expected: 0 },
    { size: 10, downloaded: 10, status: 'DOWNLOADING', expected: 0 }
  ]
  tests.forEach(({ size, downloaded, status, expected }) => {
    it(`should return ${expected} when the value is ${downloaded}/${size} and ${status}`, function () {
      assert.strictEqual(determineRemaining(size, downloaded, status), expected)
    })
  })
})

describe('Validate Size', function () {
  const tests = [
    { size: 0, expected: 0 },
    { size: Math.pow(2, 31) + 1, expected: Math.pow(2, 31) - 1 },
    { size: 10, expected: 10 }
  ]
  tests.forEach(({ size, expected }) => {
    it(`should return ${expected} when the value is ${size}`, function () {
      assert.strictEqual(validateSize(size), expected)
    })
  })
})
