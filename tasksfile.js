import tasks from 'tasksfile'
const { sh, cli } = tasks

/**
 * Runs jsdoc with optional config
 * @param {*} options An object with the arguments passed from cli
 */
function doc (options) {
  const projectDefaults = {
    src: [
      './app/*',
      './README.md'
    ],
    config: './jsdoc-config.json',
    dest: 'docs',
    template: 'node_modules/jsdoc-fresh'
  }
  const customSrc = options.src || []
  const src = [...projectDefaults.src, ...customSrc]
  const config = options.config || projectDefaults.config
  const dest = options.dest || projectDefaults.dest
  const template = options.template || projectDefaults.template
  sh(`npx jsdoc ${src.join(' ')} -c ${config} -d ${dest} -t ${template}`)
}

cli({
  doc
})
