FROM node:14.17.0-alpine3.13

LABEL maintainer="admin@casey-vockrodt.com"  \
      version="1.0.0" \
      source="git@gitlab.com:casey.vockrodt/putio_client.git" \
      description="utorrent proxy client for put.io" \
      name="putio_client"

ADD app /app
COPY package.json /app/package.json

WORKDIR /app
RUN npm install

CMD npm start
