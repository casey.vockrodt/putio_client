'use strict'

import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import config from 'config'
import routes from './routes/api.js'

const { urlencoded, json } = bodyParser

console.debug('Using api key: ' + config.get('apiKey'))

const app = express()

app.disable('x-powered-by')

app.use(cookieParser())
app.use(urlencoded({
  extended: true
}))
app.use(json())
app.use(methodOverride())

app.use('/', routes)

const port = config.get('web.port')
app.listen(port, function () {
  console.info('The server is running on port %s', port)
})
