import express from 'express'
import config from 'config'
import readTorrent from 'read-torrent'
import PutioAPI from '@putdotio/api-client'
import { determineStatusValue, validateSize, validateEta, validateAvailability, determineRemaining } from '../util.js'

export default (function () {
  const app = express.Router()
  const putioAPI = new PutioAPI.default({ clientID: config.get('putio.id') }) // eslint-disable-line new-cap
  putioAPI.setToken(config.get('putio.token'))
  const parentFolder = putioAPI.Files.Search(
    config.get('putio.folder'),
    { fileType: 'FOLDER' }
  ).files[0].id

  app.get('/gui/token.html', function (req, res) {
    authenticate(res)
  })

  app.get('*', function (req, res, next) {
    verifyToken(req, next, res)
  })

  app.post('*', function (req, res, next) {
    verifyToken(req, next, res)
  })

  app.post('/gui/', function (req, res) {
    console.debug('POST')
    switch (req.query.action) {
      case 'add-file':
        return addFile(req, res, putioAPI, parentFolder)
      default:
        return invalidAction(req, res)
    }
  })

  app.get('/gui/', function (req, res) {
    console.debug('GET')
    if (req.query.list === '1') {
      return list(putioAPI, parentFolder, res)
    }
    switch (req.query.action) {
      case 'add-url':
        return addUrl(req, res, putioAPI, parentFolder)
      case 'getsettings':
        return getsettings(res)
      case 'remove':
        return remove(putioAPI, req, res)
      case 'removedata':
        return removedata(putioAPI, req, res)
      case 'setsetting':
      case 'getprops':
      case 'setprops':
      case 'getfiles':
      case 'start':
      case 'stop':
      case 'pause':
      case 'unpause':
      case 'forcestart':
      case 'recheck':
      case 'setprio':
      case 'queuebottom':
      case 'queuedown':
      case 'queuetop':
      case 'queueup':
        return invalidAction(req, res)
      default:
        return invalidAction(req, res)
    }
  })

  app.get('*', function (req, res) {
    console.warn('Attempting an unknown route...')
    res.send({
      error: 'Unknown Route'
    })
  })

  return app
})()

function authenticate (res) {
  console.debug('Authenticating...')
  res.send('<div id="token" style="display:none;">' + config.get('apiKey') + '</div>')
}

function verifyToken (req, next, res) {
  console.debug('Verifying token...')
  if (req.query.token === config.get('apiKey')) {
    console.debug('Success!')
    next()
  } else {
    const msg = `Incorrect API key: ${req.query.token}. Expected: ${config.get('apiKey')}`
    console.error(msg)
    res.send({ error: msg })
  }
}

function addFile (req, res, putioAPI, parentFolder) {
  console.debug('Adding torrent from file...')
  readTorrent(req.files.torrent_file.path, function (err, torrent) {
    if (err) {
      console.error(err)
      return res.status(500).end()
    }
    putioAPI.Transfers.Add(torrent.infoHash, parentFolder)
    console.info('Added ' + torrent.name)
  })
  return res.end()
}

function addUrl (req, res, putioAPI, parentFolder) {
  console.debug('Adding torrent from url...')
  readTorrent(req.query.s, function (err, torrent) {
    if (err) {
      console.error(err)
      return res.status(500).end()
    }
    putioAPI.Transfers.Add(req.query.s, parentFolder)
      .then(r => console.info('Added transfer: ' + r))
      .catch(e => console.error('An error occurred while adding transfer: ', e))
  })
  return res.send({})
}

function getsettings (res) {
  console.debug('Getting settings...')
  return res.send({
    build: 44632,
    settings: [
      [
        'install_modification_time',
        0,
        '0',
        { access: 'Y' }
      ]
    ]
  })
}

function remove (putioAPI, req, res) {
  console.debug('Removing torrent...')
  putioAPI.Transfers.Cancel([req.query.hash])
  console.info('Removed torrent ' + req.query.hash)
  return res.status(200).end()
}

function removedata (putioAPI, req, res) {
  console.debug('Removing torrent and data...')
  putioAPI.Transfers.Cancel([req.query.hash])
  console.info('Removed torrent and data' + req.query.hash)
  return res.status(200).end()
}

function list (putioAPI, parentFolder, res) {
  console.debug('Listing torrents...')
  putioAPI.Transfers.Query()
    .then(function (data) {
      const retData = {
        build: 44994,
        torrents: [],
        label: [[config.get('putio.folder'), parentFolder]],
        torrentc: '994925276',
        rssfeeds: [],
        rssfilters: []
      }
      for (const transferIndex in data.transfers) {
        const transfer = data.transfers[transferIndex]
        const detail = [
          transfer.id,
          determineStatusValue(transfer.status),
          transfer.name,
          validateSize(transfer.size),
          transfer.percent_done * 10,
          transfer.downloaded || 0,
          transfer.uploaded || 0,
          Math.round(transfer.current_ratio * 100),
          transfer.up_speed || 0,
          transfer.down_speed || 0,
          validateEta(transfer.estimated_time),
          config.get('putio.folder'),
          transfer.peers_connected || 0,
          transfer.peers_getting_from_us + transfer.peers_sending_to_us,
          transfer.peers_connected || 0,
          transfer.peers_sending_to_us || 0,
          validateAvailability(transfer.availability, transfer.size),
          transfer.id,
          determineRemaining(transfer.size, transfer.downloaded, transfer.status),
          '',
          '',
          transfer.status,
          '1',
          1550730519,
          0,
          '',
          config.get('putio.download_dir'),
          0,
          '4767C3CE'
        ]
        retData.torrents.push(detail)
      }
      return res.json(retData)
    })
}

function invalidAction (req, res) {
  console.warn('invalid endpoint: ' + req.query.action)
  console.warn('query parameters: ' + req.query)
  return res.send('{"status":"unknown"}')
}
