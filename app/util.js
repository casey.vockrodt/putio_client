export function determineStatusValue (status) {
  let statusValue = 16
  switch (status) {
    case 'DOWNLOADING':
      statusValue = 1
      break
    case 'COMPLETING':
    case 'SAVING':
      statusValue = 130
      break
    case 'IN_QUEUE':
    case 'WAITING':
      statusValue = 64
      break
    case 'COMPLETED':
    case 'SEEDING':
    case 'PREPARING_SEED':
      statusValue = 136
      break
    default:
      statusValue = 16
  }
  return statusValue
}

export function validateEta (eta) {
  if (eta === null) {
    return 0
  }
  return eta
}

export function validateAvailability (availability, total) {
  const MAX_INT32 = Math.pow(2, 31) - 1
  if (availability === null) {
    if (total > MAX_INT32) {
      return MAX_INT32
    }
    return total
  } else if (availability > MAX_INT32) {
    return MAX_INT32
  }
  return availability
}

export function determineRemaining (size, downloaded, status) {
  if (status === 'COMPLETED') {
    return 0
  }
  const remaining = size - downloaded
  const MAX_INT32 = Math.pow(2, 31) - 1
  if (remaining > MAX_INT32) {
    return MAX_INT32
  }
  return remaining
}

export function validateSize (size) {
  const MAX_INT32 = Math.pow(2, 31) - 1
  if (size > MAX_INT32) {
    return MAX_INT32
  }
  return size
}
